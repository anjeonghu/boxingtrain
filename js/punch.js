/**
 * Created by front on 2019-08-09.
 */
let button = document.getElementById('startButton')
let punch = document.getElementById('punch')
let container = document.getElementById('video-container')
let gameDuration = getRandomArbitrary(1, 10) * 1000;
const video = document.querySelector('video');
const hitOverlay = document.querySelector('#hit-overlay');
const displaySize = { width: video.clientWidth, height: video.clientHeight }
let canvas = null;
let game;
let pos = {x:0, y:0};
let punchLeft = 0;
let punchTop = 0;
let hittingArea = 80;
let punchAni, opacityAni, isHit;

faceapi.nets.tinyFaceDetector.loadFromUri('/boxingTrain/models')

function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}

function punchTrainning () {
    clearTimeout(punchAni);
    clearTimeout(opacityAni);
    clearTimeout(isHit);
    clearTimeout(game);

    let punchDuration = getRandomArbitrary(0.1, 0.5);
    let locationX = getRandomArbitrary(0, (container.offsetWidth - 20));
    let locationY = getRandomArbitrary(0, (container.offsetHeight / 2));
    let opacityDuration = 0;
    punch.style.opacity = 1;
    punch.style.transform = 'scale(0.5)';

    punchAni = setTimeout (async () => {
        punchLeft = (pos.x !== 0) ? pos.x + 'px' : (container.offsetWidth/2) - (punch.offsetWidth / 2) + 'px';
        punchTop = (pos.y !== 0) ? pos.y + 'px' : '20%';

        punch.style.transform = 'translateZ(0px)';
        punch.style.transitionDuration  = `${punchDuration}s`;
        punch.style.transform = 'scale(1.2)';
        punch.style.left = punchLeft;
        punch.style.top = punchTop;
    }, 200);


    isHit = setTimeout(function () {
        if (pos.x < (parseInt(punchLeft) + hittingArea) && pos.x > (parseInt(punchLeft) - hittingArea) && pos.y <  (parseInt(punchTop) + hittingArea) && pos.y >  (parseInt(punchTop) - hittingArea)) {
            console.log('hit !!!!!!!!!!!!!!!!!!!!!!!!!!!!')
            hitOverlay.classList.add('hit');
        }
    }, (punchDuration * 1000) + 200);


    opacityDuration = (punchDuration * 1000) + 300;
    opacityAni = setTimeout(function () {
        hitOverlay.classList.remove('hit');
        punch.style.transitionDuration  = `0s`;
        punch.style.opacity = 0;
        punch.style.left = `${locationX}px`;
        punch.style.top = `${locationY}px`;
        gameDuration = getRandomArbitrary(1, 5) * 1000;
        game = setTimeout(punchTrainning, gameDuration);
    }, opacityDuration + 200)

}


button.addEventListener('click', function () {

    canvas = faceapi.createCanvasFromMedia(video);
    container = document.querySelector('#video-container');
    container.appendChild(canvas);
    faceapi.matchDimensions(canvas, displaySize)

    game = setTimeout(punchTrainning, gameDuration);

    setInterval(async () => {
        const detections = await faceapi.detectAllFaces(video, new faceapi.TinyFaceDetectorOptions())

        if (detections.length > 0){
            let rx = container.clientWidth / detections[0].imageDims.width;
            let ry = container.clientHeight / detections[0].imageDims.height;
            pos.x = (container.clientWidth - (detections[0].box.topLeft.x * rx) - 200);
            pos.y = (detections[0].box.topLeft.y * ry);

        } else {
            pos.x = 0;
            pos.y = 0;
        }
        const resizedDetections = faceapi.resizeResults(detections, displaySize)
        canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height)
        faceapi.draw.drawDetections(canvas, resizedDetections)
    }, 100)


}, false)